import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';

import { EmailComposer } from 'ionic-native';

@Component({
  selector: 'page-alaric-quachero',
  templateUrl: 'alaric-quachero.html'
})
export class AlaricQuacheroPage {

  constructor(public navCtrl: NavController) {

  }
	mail = {
		subject: [''],
		message: ['']
	}

	formMail() {
	let email = {
		to: 'alaricquachero@gmail.com',
		subject: this.mail.subject.toString(),
		body: this.mail.message.toString()
	};
	EmailComposer.open(email);
	}
}
