import { Component } from '@angular/core';

import { HomePage } from '../home/home';
import { ProjectPage } from '../project/project';
import { TeamPage } from '../team/team';
import { ContactPage } from '../contact/contact';
import { SettingPage } from '../setting/setting';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {
  // this tells the tabs component which Pages
  // should be each tab's root Page
  tab1Root: any = HomePage;
  tab2Root: any = ProjectPage;
  tab3Root: any = TeamPage;
  tab4Root: any = ContactPage;
  tab5Root: any = SettingPage;

  constructor() {

  }
}
