import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';

import { TranslateService } from 'ng2-translate';

import { AlertController } from 'ionic-angular';

@Component({
  selector: 'page-setting',
  templateUrl: 'setting.html'
})
export class SettingPage {

  constructor(public navCtrl: NavController, public translate: TranslateService, private alertCtrl: AlertController) {

  }

  changeLangue(langue){
  	this.translate.use(langue);

  	let alert = null;

  	switch (langue) {
  		case "fr":
  			alert = this.alertCtrl.create({
			    title: 'Langage',
			    subTitle: 'Vous avez choisi la langue française',
			    buttons: ['Ok']
	  	});
  			break;
  		case "en":
	  		alert = this.alertCtrl.create({
			    title: 'Language',
			    subTitle: 'You have choose English language',
			    buttons: ['Dismiss']
		  	});
  			break;
  	}
  	alert.present();
  }
}
