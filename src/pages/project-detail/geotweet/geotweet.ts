import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';


@Component({
  selector: 'page-geotweet',
  templateUrl: 'geotweet.html'
})
export class GeotweetPage {

  constructor(public navCtrl: NavController) {

  }
}
